let clusters = []

function setup() {
  createCanvas(windowWidth, windowHeight)
  background(0)
  angleMode(DEGREES)

  stroke(255) 
  strokeWeight(1)

  for (let i = 0; i < 8; i++) {
    let x = random(width)
    let y = random(height)
    let c = new Cluster(x, y)
    clusters.push(c)    
  }
}

function draw() {
  background(0)
  for (const cluster of clusters) {
    cluster.display()
    cluster.update()
  }
}

class Cluster {
  constructor(x, y) {
    this.location = createVector(x, y)
    this.t = random(360)
    this.angleSpeed = random(-2, 2)

    this.lines = []

    let sd = random(30, 100)
    for (let i = 0; i < 200; i++) {
      let x = randomGaussian(0, sd)
      let y = randomGaussian(0, sd)
      let lin = new Line(x, y)
      this.lines.push(lin)
    } 

    this.noiseStart = random(0, 1000)
    this.dxStart = random(0, 1000)
    this.dyStart = random(0, 1000)
  }

  display() {
    let o = 255 * noise((frameCount + this.noiseStart) * 0.05)
    stroke(255, o)
    push()
    let dx = sin(frameCount + this.dxStart) * 300
    let dy = sin(frameCount + this.dyStart) * 300
    translate(this.location.x + dx, this.location.y + dy)
    rotate(this.t)
    for (const lin of this.lines) {
      lin.display()
      lin.update()
    }
    pop()
  }

  update() {
    this.t += this.angleSpeed
  }
}

class Line {
  constructor(x, y) {
    this.location = createVector(x, y)
    this.l = random(2, 10)
    this.t = random(360)
    this.angleSpeed = random(-8, 8)
  }

  display() {
    push()
    translate(this.location.x, this.location.y)
    rotate(this.t)
    line(-this.l/2, 0, this.l/2, 0)
    pop()
  }

  update() {
    this.t += this.angleSpeed
  }
}
